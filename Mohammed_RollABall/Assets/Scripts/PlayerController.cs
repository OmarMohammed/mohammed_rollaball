﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    //Text for falling into the void
    public GameObject loseTextObject;

    //variables for the colors
    private float green = 0f;
    private float blue = 0f;
    private float red = 0f;
    private float trans = .2f;
    //variables to change the colors
    float greenChange = .01f;
    float blueChange = .01f;
    float redChange = .01f;
    float transChange = .01f;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();

        //turn text off
        loseTextObject.SetActive(false);
        winTextObject.SetActive(false);
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        //if count is below 100, show how many you have vs how many you need
        if (count < 100)
        {
            countText.text = "Count: " + count.ToString() + "/100";
        }
        //if count is 100 or over, tell the play to go to the stairs
        if (count >= 100)
        {
            countText.text = "Go to the top of the stairs to win!!!";
        }
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);

        //check if the player has fallen off
        if (transform.position.y <= -10)
        {
            //if so, tell them they lost
            loseTextObject.SetActive(true);
        }

        //check to see if any of the movement keys are pressed and if so change their color accordingly
        if (Keyboard.current.wKey.isPressed == true) 
        {
            //change the color slightly, repeats for all 4 colors
            blue = blue + blueChange;
            //code to make sure the color bounces between 0 and 1, repeats for all 4 colors
            if (blue >= 1f) 
            {
                blue = .99f;
                blueChange = -.01f;
            }
            if (blue <= 0)
            {
                print("hi");
                blue = .01f;
                blueChange = .01f;
            }
        }
        if (Keyboard.current.aKey.isPressed == true)
        {
            green = green + greenChange;
            if (green >= 1f) 
            {
                green = .99f;
                greenChange = -.01f;
            }
            if (green <= 0) 
            {
                green = .01f;
                greenChange = .01f;
            }
        }
        if (Keyboard.current.sKey.isPressed == true)
        {
            trans = trans + transChange;
            if (trans >= 1f) 
            {
                trans = .99f;
                transChange = -.01f;
            }
            if (trans <= 0f) 
            {
                trans = .1f;
                transChange = .01f;
            }
        }
        if (Keyboard.current.dKey.isPressed == true)
        {
            red = red + redChange;
            if (red >= 1f) 
            {
                red = .99f;
                redChange = -.01f;
            }
            if (red <= 0) 
            {
                red = .01f;
                redChange = .01f;
            }
        }
        //Change the color to the new color
        gameObject.GetComponent<Renderer>().material.color = new Color(red, green, blue, trans);
        //End of that color check
    }

    private void OnTriggerEnter(Collider other)
    {
        //if the player gets to the finish line
        if (other.gameObject.CompareTag("Finish_Line"))
        {
            //if the player gets to the finish line and has 100 points, they win
            if (count >= 100)
            {
                winTextObject.SetActive(true);
            }
        }
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count += 1;

            SetCountText();
        }
    }
}